package com.example.dagger.car;

import android.util.Log;

public class Tires {
    // bu sınıfa sahip değiliz, bu yüzden @Inject ile not ekleyemeyiz
    private static final String TAG = "Car";

    public void inflate(){
        Log.e(TAG,"Tired inflated");
    }

}
