package com.example.dagger.car;

import android.util.Log;

import javax.inject.Inject;

public class Car {

    private static final String TAG="Car";

    private Engine engine; /**  @Inject Engine engine;  normal field yapabilrdik asagidakinden farklı olsun dedik */
    private Wheels wheels;

    @Inject
    public Car(Wheels wheels,Engine engine) {
        this.wheels = wheels;
        this.engine=engine;
    }
    /** farklı bir classtan method inject ederek o lcassi cagirmadan methodu cagirdik
     * Ekrana basılma onceligi:
     * ilk olarak oncelikle inject yemiş constre ilk ekrana basılır(wheels ler)
     * sonra inject yemis method basilir
     * sonrada sınıfın kendi methodu basilir (enson)
     * */



    public void driver(){

        Log.e(TAG, "driver..");
        engine.start();
    }

    @Inject
    public void enableRemote(Remote remote){
        remote.setListener(this);
    }
}
