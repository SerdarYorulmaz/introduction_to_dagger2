package com.example.dagger.dagger;


import com.example.dagger.MainActivity;
import com.example.dagger.car.Car;

import dagger.Component;

/*
* Test
*  **/

@Component(modules = {WheelsModule.class,DieselEngineModule.class})
public interface CarComponent {

    Car getCar();

    void inject(MainActivity mainActivity);
}
