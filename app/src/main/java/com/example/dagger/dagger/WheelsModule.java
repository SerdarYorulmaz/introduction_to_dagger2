package com.example.dagger.dagger;

import com.example.dagger.car.Rims;
import com.example.dagger.car.Tires;
import com.example.dagger.car.Wheels;

import dagger.Module;
import dagger.Provides;

/**
 * üçüncü part library ait sınıflar gibi @Inject yapıcısını kullanamayan nesneler eklemek için modülleri
 * ve provider yöntemlerini kullanariz
 */

@Module
public abstract class WheelsModule {

    @Provides
    static Rims provideRims() {
        return new Rims();
    }

    @Provides
    static Tires provideTires() {
        Tires tires = new Tires();
        tires.inflate();
        return tires;
    }

    @Provides
    static Wheels provideWheels(Rims rims, Tires tires) {
        return new Wheels(rims, tires);
    }
}
